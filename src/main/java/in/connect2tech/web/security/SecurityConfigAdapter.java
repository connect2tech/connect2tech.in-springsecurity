package in.connect2tech.web.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;

@Configuration
@EnableWebSecurity
public class SecurityConfigAdapter extends WebSecurityConfigurerAdapter {

	// In memory authentication
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		UserBuilder users = User.withDefaultPasswordEncoder();
		
		//Single Role
		/*auth.inMemoryAuthentication().withUser(users.username("john").password("password").roles("EMPLOYEES"))
				.withUser(users.username("mary").password("password").roles("MANAGER"))
				.withUser(users.username("jack").password("password").roles("ADMIN"));*/
		
		//Multiple Roles
		auth.inMemoryAuthentication().withUser(users.username("john").password("test123").roles("EMPLOYEE"))
				.withUser(users.username("mary").password("test123").roles("EMPLOYEE", "MANAGER"))
				.withUser(users.username("susan").password("test123").roles("EMPLOYEE", "ADMIN"));
		
		
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// super.configure(http);


		//Login
		/*
		http.authorizeRequests().anyRequest().authenticated().and().formLogin().loginPage("/showMyLoginPage")
				.loginProcessingUrl("/authenticateTheUser").permitAll();
		 */

		// Login & Logout
		/*http.authorizeRequests().anyRequest().authenticated().and().formLogin().loginPage("/showMyLoginPage")
				.loginProcessingUrl("/authenticateTheUser").permitAll().and().logout().permitAll();
		*/
		
		// Role bases access
		/*http.authorizeRequests().
		antMatchers("/").hasRole("EMPLOYEE").
		antMatchers("/leaders/**").hasRole("MANAGER")
		.antMatchers("/systems/**").hasRole("ADMIN").
		and().formLogin().loginPage("/showMyLoginPage")
		.loginProcessingUrl("/authenticateTheUser").
		permitAll().and().logout().permitAll();*/
		
		
		//Access denied page 
		http.authorizeRequests().
		antMatchers("/").hasRole("EMPLOYEE").
		antMatchers("/leaders/**").hasRole("MANAGER")
		.antMatchers("/systems/**").hasRole("ADMIN").
		and().formLogin().loginPage("/showMyLoginPage")
		.loginProcessingUrl("/authenticateTheUser").
		permitAll().and().logout().permitAll().and().exceptionHandling().accessDeniedPage("/access-denied");
	}

}
